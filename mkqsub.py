#!/usr/bin/env python3

from __future__ import print_function
import os
# import sys
import fnmatch
import argparse
import re
# from math import log
import tempfile
import shutil

parser = argparse.ArgumentParser(
    description='General test file for writing .sub files.')
parser.add_argument('-y', '--yes', '--confirm',
                    help='This flag must be set in order to confirm command\
                    execution (and that this help was read!!!)',
                    action='store_true', required=False)
parser.add_argument('-f', '--gjfs', nargs='*',
                    help='A list of one or more *.gjf files for which to \
                    create corresponding *.sub files.', required=False)
parser.add_argument('-n', '--ntasks', help='Specify number of CPUs desired for\
                    the created sub files. Defaults to 2 CPUs', required=False)
parser.add_argument(
    '-t', '--time', help='Specify maximum job runtime for the created sub \
    files. Defaults to 168 hours.', default='168', required=False)
parser.add_argument('-p', '--path', help='Path to *.gjf files for which *.sub\
                    files will be created. (Defaults to Current Directory.)',
                    default=os.getcwd(), required=False)
parser.add_argument('--print', dest='P',
                    help='Print the list of *.sub files created',
                    action='store_true', required=False)
parser.add_argument('-r', '--recursive', help='Create *.sub files for this\
                    folder and any subdirectories within it.',
                    action='store_true', required=False)
parser.add_argument('-R', '--rwf', dest='RWF',
                    help='Do not add named RWF info to the top of gjf.',
                    action='store_true', required=False)
parser.add_argument('-c', '--cube',
                    help='Create *.sub files for a homo/lumo cubegen job.',
                    action='store_true', required=False)
# parser.add_argument('-P', '--partition',
                    # help='Cluster partition on which the job will run. \
                    # Defaults to grethor partition.',
                    # default='grethor', required=False)
args = parser.parse_args()

# Add function to specify non-default memory, --mem=(MB) or --mem-per-cpu=(MB).
# Hopefully a blank line will not cause it any issue.
# Check to handle this in any case.
# We think we have said function in place. See getmem() below.
# TODO: clean up main code/actually *have* a main() loop, perhaps.


newgjf = """%RWF={0}-Gau.rwf
%Int={0}-Gau.int
%D2E={0}-Gau.d2e
%NoSave
"""

qsubcontent = """#!/bin/bash

#PBS -l walltime={1}:00:00
#PBS -l nodes=1:ppn={2}:pfsdir
#PBS -l mem={3}MB
#PBS -N {0}
#PBS -j oe
#PBS -o {0}.out
trap "cd $PBS_O_WORKDIR;mkdir $PBS_JOBID;cp -p $PFSDIR/*.chk $PFSDIR/*.rwf $PBS_JOBID;exit" TERM
module load gaussian/g09e01
cd $PBS_O_WORKDIR
cp {0}.gjf $PFSDIR
cd $PFSDIR
\\time -f '%Us User Time\\n%Ss System Time\\n%E Total Time Elapsed\\n%P CPU\\n%M Max Resident Set Size (kB)' g09 < {0}.gjf > $PBS_O_WORKDIR/{0}.log
cp -a {0}.* $PBS_O_WORKDIR
"""

content = """#!/bin/bash
#SBATCH --job-name={0}
#SBATCH --ntasks={2}
#SBATCH --nodes=1
#SBATCH --mem-per-cpu={3}
#SBATCH --time=0-{1}:00:00
#SBATCH --export=all
#SBATCH --output={0}.out
#SBATCH --error={0}.out
#SBATCH --partition={4}
module unload gaussian
module load gaussian-09-e.01
\\time -f '%Us User Time\\n%Ss System Time\\n%E Total Time Elapsed\\n%P CPU\\n%M Max Resident Set Size (kB)' g09 {0}.gjf
echo "divide max RSS size by 4 to get real value due to bug in time command."
"""

cubegen = """#!/bin/bash
#SBATCH --job-name={0}_cubegen
#SBATCH --ntasks={2}
#SBATCH --nodes=1
#SBATCH --mem-per-cpu={3}
#SBATCH --time=0-{1}:00:00
#SBATCH --export=all
#SBATCH --output={0}_cubegen.out
#SBATCH --error={0}_cubegen.out
module load gaussian
echo "running formchk..."
\\time -f '%Us User Time\\n%Ss System Time\\n%E Total Time Elapsed\\n%P CPU' formchk {0}.chk {0}.fchk
echo "running cubegen..."
\\time -f '%Us User Time\\n%Ss System Time\\n%E Total Time Elapsed\\n%P CPU' cubegen 0 mo=homo,lumo {0}.fchk {0}.cub 0
echo "cleaning up formchk..."
\\time -f '%Us User Time\\n%Ss System Time\\n%E Total Time Elapsed\\n%P CPU' rm {0}.fchk
"""


def getgjf(path):
    """Is the location specified local or recursive?"""
    if args.recursive:
        for root, dirs, files in os.walk(path):
            for name in files:
                if fnmatch.fnmatch(name, '*.gjf'):
                    yield os.path.join(root, name)
    elif args.gjfs:
        for files in args.gjfs:
            if os.path.isfile(files):
                if fnmatch.fnmatch(files, '*.gjf'):
                    yield files
    else:
        if os.path.isfile(path):
            yield path
        for files in os.listdir(path):
            if fnmatch.fnmatch(files, '*.gjf'):
                yield os.path.join(path, files)


def getchk(path):
    """Is the location specified local or recursive?"""
    if args.recursive:
        for root, dirs, files in os.walk(path):
            for name in files:
                if fnmatch.fnmatch(name, '*.chk'):
                    yield os.path.join(root, name)
    elif args.gjfs:
        for files in args.gjfs:
            if os.path.isfile(files):
                if fnmatch.fnmatch(files, '*.chk'):
                    yield files
    else:
        if os.path.isfile(path):
            yield path
        for files in os.listdir(path):
            if fnmatch.fnmatch(files, '*.chk'):
                yield os.path.join(path, files)


def getntasks(path):
    if not args.ntasks:
        with open(path) as f:
            for line in f:
                if line.lower().startswith(("%nprocshared", "%nproc")):
                    ntasks = line.rsplit("=")[1].strip()
                    return ntasks
                # else:
                #     ntasks = args.ntasks
                #     print("hit the inner args.ntasks Check")
                #     print(ntasks)
                #     break
                # We'll leave this here, in case we want to change to this functionality.
                # This one runs per file, and only sets the user supplied value if it is missing.
            else:
                ntasks = 2  # When in doubt, ask for 2 cpu.
                return ntasks
    else:
        ntasks = args.ntasks
        return ntasks
        # this version overrides the check entirely,
        # as we hope it only gets used with a specific gjf...

# def setmem(path):
    # with open(path) as f:
        # for line in f:


def getmem(path):
    membytes = ""  # set this as a dummy variable to get the def to start.
    with open(path) as f:
        for line in f:
            if line.lower().startswith(("%mem")):
                mem = line.rsplit("=")[1].strip()
                # find the number in 2GB, 4GB, etc.
                x = (re.findall("\d", mem))
                # find the GB in 2GB, 4GB, etc.
                y = (re.findall("[mgtMGT][bB]", mem))
                memint = int(''.join(str(i) for i in x))
                membytes = ''.join(i for i in y).lower()
                break
            else:
                # if we don't set any memory in the gjf,
                # set the value to 1024 MB as a failsafe.
                memint = max(1, 1 * int(getntasks(path)))
                membytes = "gb"
    if "tb" in membytes:
        print(str(
            "you asked for a terabyte of ram (or more!),\
            check {0} for errors!").format(path))
        exit(1)
    elif "gb" in membytes:
        # if we find a GB or gb in the %mem, multiply it by 1024 to get it into MB,
        # and return the value. (As an int so as to not piss off sbatch.)
        return int(memint * 1024 + int(128))
    elif "mb" in membytes:
        return int(int(memint) + int(128))
    else:
        print(str("No memory directive was found in {0}.").format(path),
              int((memint) / int(getntasks(path)) + 50), str(
            "MB of RAM per CPU (nprocshared = {0}) has been allocated. Is "
            "this what you wanted?").format(getntasks(path)))
        # otherwise, return the value in MB, presumably.
        # return int(int(memint) / int(getntasks(path)) + 50)
        return int(int(memint) + int(128))


def writeqsub(path):
    """local qsub writer"""
    for gjf in getgjf(path):
        with open(gjf.rsplit(".", 1)[0] + ".qsub", 'w') as localout:
            if args.P:
                print(gjf.rsplit(".", 1)[0] + ".qsub")
            localout.write(qsubcontent.format(os.path.basename(gjf).rsplit(".", 1)[0], args.time, getntasks(gjf), getmem(gjf)))


def writesub(path):
    """local sub writer"""
    for gjf in getgjf(path):
        with open(gjf.rsplit(".", 1)[0] + ".sub", 'w') as localout:
            if args.P:
                print(gjf.rsplit(".", 1)[0] + ".sub")
            localout.write(content.format(
                os.path.basename(gjf).rsplit(".", 1)[0],
                args.time, getntasks(gjf), getmem(gjf), args.partition))


def writecubesub(path):
    for chk in getchk(path):
        with open(chk.rsplit(".", 1)[0] + "_cubegen.sub", "w") as localout:
            if args.P:
                print(chk.rsplit(".", 1)[0] + "_cubegen.sub")
            localout.write(cubegen.format(
                os.path.basename(chk).rsplit(".", 1)[0], args.time, "1", "128"))


def writerwfgjf(path):
    stop = 0
    fp = tempfile.NamedTemporaryFile()
    for gjf in getgjf(path):
        with open(gjf) as original, open(fp.name, mode='w') as tmpF:
            tmpF.write(newgjf.format(
                os.path.basename(gjf).rsplit(".", 1)[0]))
            for line in original:
                if "rwf" in line.lower():
                    stop = 1
                    break
                    # continue
                else:
                    tmpF.write(line)
            tmpF.flush()
            if stop is 0:
                shutil.copyfile(fp.name, gjf)
            stop = 0


if args.yes:
    if args.gjfs:
        if args.cube:
            writecubesub(args.gjfs)
        else:
            writeqsub(args.gjfs)
        if not args.RWF:
            writerwfgjf(args.gjfs)
    elif os.path.isdir(args.path):
        if args.cube:
            writecubesub(args.path)
        else:
            writeqsub(args.path)
        if not args.RWF:
            writerwfgjf(args.path)
    elif not args.path:
        if args.cube:
            writecubesub(os.getcwd())
        else:
            writeqsub(os.getcwd())
        if not args.RWF:
            writerwfgjf(os.getcwd())
    else:
        print("That is not a valid path!!!")
else:
    parser.print_help()
