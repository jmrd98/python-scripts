README

loglog.sh:

This script scans the home directory for *.log files, displays their path and last modified date. The list is sorted ascending, such that the newest entries are last.
By default, we list all logs modified in the last 24 hours. Optionally, a number can appended to the command to change the time interval.

Usage: loglog.sh (number)
Example: loglog.sh; or, loglog.sh 3 (this displays logs modified in the preceding 3*24 hours = 72 hr = 3 days.). A shorter time is valid as well: loglog.sh 0.25 
will yield files modified in the preceding six hours (24*0.25, or 24/4 = 6).

***NOTE*** As of 5/27/16, the Forge Cluster has a bug in their timezones such that the UTC correction is being applied twice. The result is that file modified dates
as listed on the cluster are five hours behind actual clock time. e.g., a file modified at 3 PM, will be listed as being modified at 10 AM. Use care when interpreting 
the results of this script.


chkfilenamestrip.sh:

This script acts on a gjf file and removes the windows style checkpoint path. This is needed to use the file at the cluster. A copy of the gjf is created named (file).gjf.origCHK if for some reason there is a mistake. (See origrm.sh below.)

Usage: chkfilenamestrip.sh [gjf]
Example: chkfilenamestrip.sh example.gjf; or, chkfilenamestrip.sh *.gjf (This acts on all gjf files in the current directory.)


mkM062X.sh:

This script acts on a gjf file and replaces any instance of b3lyp with m062x. This is convenient for use with GaussView 03, which does not support m062x as a basis set. A copy of the gjf is created named (file).gjf.origBS if for some reason there is a mistake. (See origrm.sh below.)

Usage: Create gjf file in GV03 as normal, selecting b3lyp with any other settings desired. Run the script with the gjf file as an argument once on the cluster.
Example: mkM062X.sh example.gjf; or, mkM062X.sh *.gjf (This acts on all gjf files in the current directory.)


origrm.sh:

This file simply removes all "orig" type backup files from a directory.

Usage: Simply run from inside the directory of interest. No arguments are needed or taken.

sb:

This program takes a sub file, or more commonly many sub files and submits them for execution using sbatch at once.

Usage: sb.sh example.sub; or, sb.sh sub1.sub sub2.sub sub3.sub ... subn.sub; or, sb.sh *.sub.

sq:

This prgram is a shortcut to squeue. It restricts the view to your own jobs, and widens the job title field such that it is sane to read most jobs. Optionally, if a number is appended, it sets the width of the job title field to that number of characters.

Usage: sq; or, sq 45.


mksub.py:

This file takes a given gjf file and generates a suitable sub file needed for execution on the cluster.

Usage and help are available when the program is run. For most purposes, all that should be required is mksub.py -y for sensible defaults.


fixatoman.sh:

This file acts on a log file, and fixes the instances of Atom  AN in the frequency calculations such that GV03 can read the information. A backup file named file.log.origAN is created. (See origrm.sh above)

Usage: fixatoman.sh example.log; or, fixatoman.sh *.log (for all log files.)


allinone.sh:

This is a metascript meant for quickly setting up files to be run on the cluster. mkM062X.sh, chkfilenamestrip.sh, mksub.py and origrm.sh are all called on all gjf files in the current directory. No arguments are needed (or taken.)


updatescripts.sh:

This shell script is a shortcut to update the deployed scripts in ~/bin/ It can be run from anywhere.
