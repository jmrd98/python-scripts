#!/bin/bash

for i in $@
do
cp -a $i $i.origBS
sed -i -e 's/pm3/pm6/' $i
touch -r $i.origBS $i
done
