#!/usr/bin/perl
use warnings;
use strict;
use Switch;
use Math::Trig;

#Script to read Gaussian pm6 optimizations and generate a list of the lowest energy structures in each hydrogen bonding category
#Command line execution format: perl log2hond.plx SEQ > OutputFile.
#Script expect log file names to be the format: SEQ_pm6optIndex.log
#Where SEQ,OutputFile,Index can be determined by the user.
#Column one of output is the index of the lowest energy structure in each group
#Column two is the number of hydrogen bonds on the structure group
#Column three is the list of h-bonds in the group.
#Column four in the energy in hartrees of lowest energy structure in each group
#Output is not sorted, but can be sorted by importing to spreadsheet

my $seq     = $ARGV[0];
my $outFile = $ARGV[1];
if ( $seq eq "" ) { die "No sequence variable defined \n"; }
my $prefix = $seq . "-pm6_opt";
#my $prefix = $seq . "-m06_sp";
my $logfile;
my $bit1 = 0;
my $bit2 = 0;
my $bit3 = 0;
my $t;
# my $set;
my @input;
my $NAtoms = 0;
# my $traj;
# my $n;
my $i;
my $j;
my $k;
my $l;
my @x;
my @y;
my @z;
my $ax;
my $ay;
my $az;
my $bx;
my $by;
my $bz;
my $ma;
my $mb;
my $dot;
my @atomtype;
my $bondLength;
my $angle;
my $donor;
my $h;
my $acceptor;
my $hbonds = "";
my $b;
my $energy;
my $numHbonds;
my %seen;
my @A;
my @B;
my @C;
my @D;

# if ( -e $outFile ) { die "h-bond output file already exists" }
open OUT, ">> $outFile" or die "could not open output file";
system("ls $prefix*.log > pm6list.csv");
if ( -z "pm6list.csv" ) { die "pm6 optimization files not found" }

# -z tests for zero length file.
open LIST, "pm6list.csv" or die "could not open pm6 list file";

# open pm6list.csv as LIST
while (my $logfile = <LIST>) {
	# $logfile = $_;
	# print "$_ local stack \n";

    # $_ seems to be the default local variable
    chomp($logfile);

    # $logfile.strip()
    $t = substr $logfile, 0, length($prefix);
    print "Step 1: $t\n";
    print "$prefix\n";
      # substr EXPR, OFFSET, LENGTH, REPLACEMENT
    $t = substr $logfile, length($prefix), length($logfile) - length($prefix);
    print "Step 2: $t\n";
    $t = substr $t, 0, length($t) - 4;
    print "Step 3: $t\n";

      if ( -e $logfile ) {
        print "$logfile \n";
        $NAtoms    = 0;
        $bit1      = 0;
        $bit2      = 0;
        $hbonds    = "";
        $numHbonds = 0;
        open IN, $logfile or die "could not open log file";
        while (<IN>) {
	    if (/^\s#T*(?:(?!opt).)*$/) { $bit3 = 1; print "hello";}
	    if ( (/Standard orientation:/) && $bit3 ) { $bit2 = 1; } 
            if (/SCF D/) { @input = split; $energy = $input[4]; }
	    if (/Stationary point found./) { $bit1 = 1; }
	    if ( (/Standard orientation:/) && $bit1 ) { $bit2 = 1; }
	    #if ( (/Standard orientation:/) )  { $bit2 = 1; }
            if ( $bit2 > 0 ) {
                @input = split;
                if ( ( defined $input[0] ) && ( defined $input[1] ) ) {
                    if (   ( $input[0] =~ /^-?\d+$/ )
                        && ( $input[1] =~ /^-?\d+$/ ) 
			&& ( $input[2] =~ '0' ) 
		)
                    {
                        switch ( $input[1] ) {
                            case 1  { $atomtype[$NAtoms] = "H" }
                            case 6  { $atomtype[$NAtoms] = "C" }
                            case 7  { $atomtype[$NAtoms] = "N" }
                            case 8  { $atomtype[$NAtoms] = "O" }
                            case 15 { $atomtype[$NAtoms] = "P" }
			    case 11 { $atomtype[$NAtoms] = "Na" }
			    else    { die "Unknown Atom" }
                        }
			print "$input[1]\n";
                        $x[$NAtoms] = $input[3];
                        $y[$NAtoms] = $input[4];
                        $z[$NAtoms] = $input[5];
                        $NAtoms++;
			
		}
                    #} else {
			    #last;
		    #}
                }
            }
	    if ( (/SCF D/) && $bit3 ) { @input = split; $energy = $input[4]; }
        }
        for ( $i = 0 ; $i < $NAtoms ; $i++ ) {
            if ( $atomtype[$i] eq "H" ) {
                for ( $j = 0 ; $j < $NAtoms ; $j++ ) {
                    if ( ( $atomtype[$j] eq "O" ) || ( $atomtype[$j] eq "N" ) )
                    {
                        $bondLength =
                          sqrt( ( $x[$i] - $x[$j] )**2 +
                              ( $y[$i] - $y[$j] )**2 +
                              ( $z[$i] - $z[$j] )**2 );
                        if ( $bondLength < 1.2 ) {
                            for ( $k = 0 ; $k < $NAtoms ; $k++ ) {
                                if (   ( $atomtype[$k] eq "O" )
                                    || ( $atomtype[$k] eq "N" ) )
                                {
                                    $bondLength =
                                      sqrt( ( $x[$i] - $x[$k] )**2 +
                                          ( $y[$i] - $y[$k] )**2 +
                                          ( $z[$i] - $z[$k] )**2 );
                                    if (   ( 1.2 < $bondLength )
                                        && ( $bondLength < 3.0 ) )
                                    {
                                        $ax = $x[$j] - $x[$i];
                                        $ay = $y[$j] - $y[$i];
                                        $az = $z[$j] - $z[$i];
                                        $bx = $x[$k] - $x[$i];
                                        $by = $y[$k] - $y[$i];
                                        $bz = $z[$k] - $z[$i];
                                        $ma = sqrt( $ax**2 + $ay**2 + $az**2 );
                                        $mb = sqrt( $bx**2 + $by**2 + $bz**2 );
                                        $dot =
                                          $ax * $bx + $ay * $by + $az * $bz;
                                        $angle = acos( $dot / ( $ma * $mb ) );

					if ( $angle > 1.8 ) {
						#if ( $angle > 2.5 ) {
                                        #if ( $angle > 2.14 ) {
                                            $numHbonds++;
                                            $donor    = $j + 1;
                                            $h        = $i + 1;
                                            $acceptor = $k + 1;
                                            $b =
                                              "$atomtype[$j]$donor-$atomtype[$i]-$atomtype[$k]$acceptor;";
                                            $hbonds .= $b;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if ( defined $hbonds ) {
            unless ( $seen{$hbonds} ) {
                $seen{$hbonds} = 1;
                push( @A, $t );
                push( @B, $numHbonds );
                push( @C, $hbonds );
                push( @D, $energy );
            }

            # print "$t \t $numHbonds \t $hbonds \t $energy \n";
            # Here we fish out only the lowest energy based on families of like hydrogen bonding... Clever girl...
            if ( $seen{$hbonds} ) {
                $l = 0;
                foreach (@A) {
                    if ( ( $C[$l] eq $hbonds ) && ( $D[$l] > $energy ) ) {
                        $A[$l] = $t;
                        $D[$l] = $energy;
                    }
                    $l++;
                }
            }
        }
    }
}
$l = 0;
foreach (@A) {
    if ( $B[$l] == 0 ) { $C[$l] = "none"; }
    print OUT "$A[$l] \t $B[$l] \t $C[$l] \t $D[$l] \n";
    $l++;
}

