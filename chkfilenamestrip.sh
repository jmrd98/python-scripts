#!/bin/bash

for i in $@
do
cp -a $i $i.origCHK
sed -i -e 's|=.*\\\(.*\)|=\1|' $i
touch -r $i.origCHK $i
done
