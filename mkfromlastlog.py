#!/usr/bin/env python3
# mkfromlastlog.py

from __future__ import print_function
import sys
import os
import shutil

l = []

gjf = """%nprocshared={nproc}
%mem={mem}
%chk={chk}
#T {opt}({optoptions})  {basis_set}{func}{extras} {freq}({freqoptions}noraman)

{title}

{charge} {spin}
{data}

"""


class format_dict(dict):
    def __missing__(self, key):
        return ""

# extdict = {}
# with open('option.cfg', 'r') as cfg:
#     extdict = eval(cfg.read())


title = "Created from " + str(os.path.basename(sys.argv[1]))
nprocshared = "2"
mem = "4gb"
optoptions = ['', 'rcfc', 'foo']
opts = {"opt": "opt"}
freqs = {"freq": "freq"}
basis_set = format_dict({"b3": "b3lyp", "m06": "m062x", "amo": "am1", "pmt": "pm3", "pms": "pm6"})
functionals = {"321": "/3-21+g", "631": "/6-31+g", "6311": "/6-311+g"}
pluses = {"p": "+", "pp": "++"}
extras = {"dp": "(d,p)"}
checkpoint = []
print(', '.join(optoptions))


def getcoord(path):
    with open(path) as f:
        s = f.readlines()
        mx = max(i for i, line in enumerate(s) if line.strip().startswith("Standard orientation"))
        # print(mx, s[mx + 5].strip())
        # print(s)
        # # print(f)
        mn = min(i for i, line in enumerate(s[mx + 5:-1]) if line.strip().startswith("----"))
        # print(mn)
        # print(mx)
        # print(mn + mx)
        for i in s[mx + 5:mx + mn + 5]:
            # print(mx)
            # print(mx + mn - 1)
            j = i.split()
            # print(i)
            l.append((j[1], j[3], j[4], j[5]))
        # for i, line in enumerate(s[mx + 5:-1]):
        #     if line.startswith(" ---"):
        #         print(mx + 5 + i - 1, s[mx + 5 + i - 1])
        # print(mx)


def passing():
    pass


def copychk(path):
    # if os.path.isfile(path.rsplit(".", 1)[0] + ".chk"):
        # shutil.copy2(path.rsplit(".", 1)[0] + ".chk", os.path.basename(sys.argv[2]).rsplit(".", 1)[0] + ".chk")
        # with open(path.rsplit(".", 1)[0] + ".chk", 'rb') as filein, open(os.path.basename(sys.argv[2]).rsplit(".", 1)[0] + "_from" + path.rsplit(".", 1)[0] + ".chk", 'wb') as fileout:
    checkpoint.append(os.path.basename(sys.argv[2]).rsplit(".", 1)[0] + ".chk")
    # fileout.write(filein)

# testprint(sys.argv[1])


data = getcoord(sys.argv[1])


output_format = format_dict({
    "opt": opts["opt"],
    "optoptions": optoptions[0],
    "freq": freqs["freq"],
    "mem": mem,
    "nproc": nprocshared,
    "basis_set": basis_set["m06"],
    "func": functionals["631"],
    "extras": extras["dp"],
    "pluses": pluses["p"],
    "title": title,
    # "chk": checkpoint[1],
    "charge": "1",
    "spin": "1",
    "data": "\n".join('\t'.join(i) for i in l)
})


def writegjf():
    if sys.argv[1] == sys.argv[2]:
        print("cannot use the input file as the output file. Check the name!")
        exit(1)
    with open(sys.argv[2], 'w') as w:
        w.write(gjf.format(**output_format))


def main(pathin):
    output_format["mem"] = "4gb"
    copychk(pathin)
    output_format["chk"] = checkpoint[0]
    getcoord(pathin)
    writegjf()

# print(l)
# writegjf()
# copychk(pathin)
main(sys.argv[1])
# print(extdict)
# print(output_format)
# print("\n".join('\t'.join(i) for i in l))
