#!/usr/bin/env python3
from __future__ import print_function
# import sys
import os
import fnmatch
import argparse
from openpyxl import Workbook
from openpyxl import load_workbook
# from openpyxl.compat import range
from openpyxl.utils import get_column_letter
from openpyxl.styles import Alignment
import tarfile
from tqdm import tqdm
from glob import glob
from itertools import accumulate
from operator import itemgetter
from collections import OrderedDict
# from decimal import *
import numpy as np
import pdb

parser = argparse.ArgumentParser(description='Scans *.log files and extracts final SCF energy and Assorted energies. Sorts and produces a XLSX file.')
parser.add_argument('-p', '--path', help='Path to *.log files which are to be scanned. (Defaults to Current Directory.)', default=os.getcwd(), required=False)
parser.add_argument('-l', '--logs', nargs='*', help='A list of one or more *.log files to scan.', required=False, action='append')
parser.add_argument('-P', '--print', dest='P', help='Print the list of *.log files scanned. (Not implimented yet.)', action='store_true', required=False)
parser.add_argument('-i', '--input', help='Specify an existing XLSX file to *APPEND* to. (Hopefully)', required=False)
parser.add_argument('-o', '--output', help='Specify a filename for the output file. (Defaults to SCFsheet.xlsx)', default='SCFsheet.xlsx', required=False)
parser.add_argument('-t', '--title', help='Title of the new worksheet in the XLSX file. (Defaults to SCFsheet)', default='SCFsheet', required=False)
parser.add_argument('-f', '--force', '--confirm', help='This flag must be set in order to confirm you want to overwrite the existing file.', action='store_true', required=False)
parser.add_argument('-e', '--errors', help="Don't run; just check for errors in the files", action='store_true')
parser.add_argument('-r', '--run', help="Confirm execution of the command. (So we can print this lovely text by default!)", action='store_true')
parser.add_argument('-q', '--quiet', help="Suppress output/run silently. (I don't know why you would want this, but I need to test the feature somehow.)", action='store_true')
parser.add_argument('-R', '--recursive', help='Scan *.log files for this folder and any subdirectories within it.', action='store_true', required=False)
args = parser.parse_args()

m = []
errors = []
mg3 = {}
# drop in a random comment...
# print(sys.version_info)


def getlog(path):
    """Is the location specified local or recursive?"""
    # if tarfile.is_tarfile(path, mode='r:*'):
    if args.recursive:
        print("we hit the recursive branch")
        for root, dirs, files in os.walk(path):
            for name in files:
                if fnmatch.fnmatch(name, '*.log'):
                    yield os.path.join(root, name)
    elif args.logs:
        # print(path)
        # print(args.logs)
        # print(glob(args.logs))
        # print("we hit the args.logs branch")
        # print(args.logs)
        for files in args.logs:
            # print(files)
            # for ind in glob(files[0]):
            for ind in files:
                # print(ind)
            # print(files)
            # print(glob(files))
                yield ind
        # for files in glob(args.logs):
            # if fnmatch.fnmatch(files, '*.tar.gz'):
                # yield "".join(files)
            # elif os.path.isfile(files):
                # if fnmatch.fnmatch(files, '*.log'):
                    # yield files
    else:
        # print(args.path)
        # print("we fell into the last branch")
        for files in os.listdir(path):
            if fnmatch.fnmatch(files, '*.log'):
                # print(os.path.join(path, files))
                yield os.path.join(path, files)


def tarscanlog(path):
    for files in getlog(path):
        print("Opening " + files + " for Reading... Please Stand By.")
        with tarfile.open(files, mode='r:*') as tar:
            print("Scanning Tarball for log files...")
            for d in tqdm(tar, total=len(tar.getnames())):
                # print(tarinfo)
                f = tar.extractfile(d)
                l = []
                s = f.readlines()
                s = [line.decode() for line in s]
                if "Normal" not in s[-1]:
                    errors.extend((os.path.basename(d.name), ": File did not end/end cleanly", "\n"))
                    continue
                scf = []
                for k in s:
                    # print(k)
                    if k.startswith(" SCF"):
                        scf.append(k)
                if not scf:
                    scf.append("SCF = 9999999")
                l.append(os.path.basename(f.name))
                l.append(d.name)
                l.append([scf[-1].split('=')[0].strip(), scf[-1].split('=')[1].strip().split()[0]])
                # this line is similarly complex to the one below (which was written first) in order to rip out the whitespace similarly.
                if [i for i in s if i.startswith(" Zero-point c")]:
                    mx = max(i for i, line in enumerate(s) if line.startswith(" Zero-point c"))
                else:
                    continue
                # this ugly ass thing replaced the commented code below: we do the comparison, and return the highest/last matching line in the file.'
                for j in s[mx:mx + 8]:
                    l.append([j.split('=')[0].strip(), j.split('=')[1].strip().split()[0]])
                    # This line is complex to catch the whitespace and the (Hartree/Particle) suffix on ZPE
                m.append(l)
    #         print(len(l), "L len")
    # print(len(m), "m len")


def scanlog(path):
    global scan
    # for files in os.listdir(path):
    for files in tqdm(getlog(path), total=len(list(getlog(path)))):
        # print(type(files))
        """stick a try/else in here???"""
        # if fnmatch.fnmatch(files, '*.log'):
        l = []
        lg3 = {}
        with open((files), 'rt') as f:
            # print(files, "printing the files in the 'with'")
            # print(f)
            s = f.readlines()
            # print(s)
            # if not e.startswith(" Normal Termination"):
            # if "Normal" not in s[-1]:
            #   errors.extend((os.path.basename(f.name), ": File did not end/end cleanly", "\n"))
            #   continue
        # for c in s:
        #     if c.startswith("IRC"):
        #         print("hit c irc branch")
        #         errors.extend((os.path.basename(f.name), ": File appears to be an IRC", "\n"))
        #     continue
            # if not s.startswith(" Zero-point"):
            #     errors.extend((os.path.basename(f.name), ": No Zero-point entry found", "\n"))
            #     print("We hit zp not in s")
            #     continue
            scf = []
            scfscan = []
            scfscanopt = []
            # print(s)
            scan = False
            # scan = True
            # if scan:
                # for k in s:
                    # if k.startswith(" SCF Done"):
                        # scfscan.append(k)
                        # if k.startswith(" Optimization completed."):
                            # scfscanopt.append(scfscan[-1])
                            # scfscan = []
                            
            for k in s:
                # print(k)
                # print("did we hit k in s?")
                if k.startswith(" SCF Done"):
                    # print('hello?')
                    scf.append(k)
                if "modred" or "modredundant" in k:
                    # print('found modred')
                    scan = True
                try:
                    if scan and (k.startswith(" Optimization completed.") or k.startswith("    -- Number of steps exceeded")):
                        scfscanopt.append(scf[-1])
                    # scf = []
                    # else:
                        # scfscanopt.append(scf[-1])
                except:
                    scfscanopt.append(scf[-1])
                # else:
                #     pass
                    # scf.append("SCF = Not_Found_,sadness")
            # print("".join(scfscanopt))
            # print(scan)
            scanpeaks = []
            maxpeak = [[-999999., -1.]]
            # print(scan)
            # print(scfscanopt)
            if scan is True:
                for i in scfscanopt:
                    # print(i)
                    scanpeaks.append(float(i.split('=')[1].strip().split()[0]))
                scanpeaks.append(float(scf[-1].split('=')[1].strip().split()[0]))
                # print(scanpeaks)
                for i, energy in enumerate(scanpeaks):
                    # print(energy, i+1)
                    try:
                        if scanpeaks[i+1] < scanpeaks[i] and scanpeaks[i-1] < scanpeaks[i]:
                            # print(energy, i, "PEAK")
                            maxpeak.append([energy, i+1])
                            # print(max(maxpeak, key=maxpeak[0]))
                        # else:
                            # # print("failed")
                            # maxpeak.append[energy, i]
                    except:
                        pass
            # print(maxpeak)
            # print(max(maxpeak))
            # exit(1)

            if not scf:
                # scf.append("SCF = Not_Found")
                scf.append("SCF = 9999999")
            l.append(os.path.basename(f.name))
            l.append(os.path.abspath(f.name))
            if scan:
                # print(l[0])
                # print(scf)
                # print(scfscanopt)
                # print(maxpeak)
                try:
                    l.append([scfscanopt[0].split('=')[0].strip(), float(scfscanopt[0].split('=')[1].strip().split()[0]), max(maxpeak)])
                except:
                    l.append(["-1", "1000", max(maxpeak)])
                    pass
                # l.append([scfscanopt[0].split('=')[0].strip(), float(scfscanopt[0].split('=')[1].strip().split()[0])])
            else:
                l.append([scf[-1].split('=')[0].strip(), float(scf[-1].split('=')[1].strip().split()[0])])
            # this line is similarly complex to the one below (which was written first) in order to rip out the whitespace similarly.
            try:
                mx = max(i for i, line in enumerate(s) if line.startswith(" Zero-point c"))
            except ValueError:
                mx = False
                pass
            try:
                g3mx = max(i for i, line in enumerate(s) if line.startswith(" Temperature="))
            except ValueError:
                g3mx = False
                pass
            # this ugly ass thing replaced the commented code below: we do the comparison, and return the highest/last matching line in the file.
            # print(mx)
            # for i, line in enumerate(s):
            #     if line.startswith(" Zero-point c"):
            # print(os.getcwd())
            # print(i)
            # for j in s[i:i + 8]:

            if mx:
                try:
                    for j in s[mx:mx + 8]:
                        l.append([j.split('=')[0].strip(), float(j.split('=')[1].strip().split()[0])])
                except ValueError:
                    pass
                # This line is complex to catch the whitespace and the (Hartree/Particle) suffix on ZPE
            if g3mx:
                try:
                    # g3 = OrderedDict()
                    g3 = dict()
                    for j in s[g3mx:g3mx + 6]:
                        j = j.strip().split()
                        if len(j) == 4:
                            # g3.update({j[0]: j[1], j[2]: j[3]})
                            g3[j[0]] = float(j[1])
                            g3[j[2]] = float(j[3])
                        if len(j) == 2:
                            g3[j[0]] = float(j[1])
                        if len(j) == 6:
                            G3MP20K = " ".join((j[0], j[1]))
                            G3MP2Energy = " ".join((j[3], j[4]))
                            g3[G3MP20K] = float(j[2])
                            g3[G3MP2Energy] = float(j[5])
                        if len(j) == 7:
                            G3MP2H = " ".join((j[0], j[1]))
                            G3MP2G = " ".join((j[3], j[4], j[5]))
                            g3[G3MP2H] = float(j[2])
                            g3[G3MP2G] = float(j[6])
                    # print(g3, l[0])

                    # print(g3['G3MP2 Energy='])
                    # g3 = OrderedDict((k.replace('=', ''), v) for k, v in g3.items())
                    # print(type(g3))
                    # print((g3))
                    g3 = {k.replace('=', ''): v for k, v in g3.items()}
                    # print(type(l))
                    # print(type(g3))
                    # print(g3)
                    # l.insert(-2, (["G3MP2 Energy", g3['G3MP2 Energy']]))
                    # l.append(["G3MP2 Energy", g3['G3MP2 Energy']])
                    l.append(["G3MP2 (0 K)", g3['G3MP2(0 K)']])
                    l.append(["G3MP2 Energy", g3['G3MP2 Energy']])
                    l.append(["G3MP2 Enthalpy", g3['G3MP2 Enthalpy']])
                    l.append(["G3MP2 Free Energy", g3['G3MP2 Free Energy']])
                    # print(g3["G3MP2 Energy"])
                    # l.g3['G3MP2 Energy'])
                    # print(type(g3))
                    # print(l)
                    # print(g3)
                    # lg3.append(l[0])
                    # lg3.append(g3)
                    lg3.update({l[0]: g3})
                    # print(l[-1])
                    pass
                            # print([i.strip() for i in j.split('=') if isinstance(i, float)])
                            # print([k.strip() for k in j.split('= ')])
                            # print(j[:24].strip().strip('='), j[26:26+12], j[36:36+10])
                            # fields = parse(j)
                            # print(parse.fmtstring, parse.size)
                except ValueError:
                    pass

            # print(list(lg3.values())[0]['G3MP2 Energy'])
            # print(lg3)
            # print(l, "\n")
            # print(type(lg3))
            # l.append(lg3)
            # l.insert(-1, 
            m.append(l)
            # print("\n")
            # print(m)
            # mg3.update(lg3)
            # if mg3:
                # print((mg3))
                # pass
            # for i in m:
                # # print(i[0])
                # if i[0] in mg3:
                    # # print(mg3[i[0]])
                    # print(tryZPE(mg3[i[0]]))
                    # # print('huzzah!')
                    # # print(mg3[i[0]]['Pressure'])
                    # pass
                # else:
                    # print('boooo!')
                    # (tryZPE(m))
                    # pass
                # pass
    # print(mg3[m[1][0]]['G3MP2 Free Energy'], m[1][0])
        # print(len(l), "L len")
    # print(m, "M list")
    # print(len(m), "m len")
# print(errors)


def checkerror():
    print("The following files contained errors and were not processed:\n", ''.join(errors))
    # print(m[0][-1])
    # ig = itemgetter(*m[-1])
    # print(ig(m))
    # print([k[-1] for k in m])
    # print(m[0][-1][m[0][0]]['E(ZPE)'])
    # if not m[-1][-1]:
        # print(m[-1][-1])
    # print(tryZPE(m))
    # print(m[0][7])
    # print(([i for i in m if 'G3MP2 Energy' in i]))
    # if any('zero' in tryZPE(i)[0] for i in m):
        # print("hellooooo")
        # print()
    # else:
        # # print(tryZPE(m)[-1])
        # print('noooooo')
    # print([(type(i), i) for i in m])
    # testdict =
    # print([i[0] for i in m])
    # for l in tqdm(sorted(m, reverse=False, key=tryZPE), total=len(m)):
        # for ll in l[2:]:
            # print(ll[0], ll[1])
        # print(l[2:], '\n')
        # pass


def tryZPE(i):
    # for i in x:
    # print(i)
    # try:
        # try:
            # print(i[0][0])
            # print([k[-1].get(i[0][0]) for k in i])
            # print([k[-1] for k in i])
            # if True:
                # print('made dict')
            # return i[0][-1][i[0][0]]['G3MP2 Energy']
                # return i[-1]['G3MP2 Energy']
            # print("hit two 2")
        # except (KeyError):
            # pass
        # pass
    try:
        if "G3MP2 (0 K)" in i[-4]:
            # print(i[-1], "hit 1")
            # print(i[-1][1])
            return i[-4]
        elif "zero-point" in i[7][0]:
            # print('made try')
            # print(i[7])
            # print(type(i[7][1]))
            return i[7]
    except IndexError:
        # print('made fail')
        # print(i[-1])
        # print(type(i[-1][1]))
        return i[-1]


def makexlsx():
    if args.input:
        if os.path.exists(args.input):
            wb = load_workbook(args.input, guess_types=False)
            ws = wb.create_sheet()  # title = args.title
            # ws= wb[args.title]
            if args.title:
                ws.title = args.title
    elif os.path.exists(args.output):
        wb = load_workbook(args.output, guess_types=False)
        ws = wb.create_sheet()  # title = args.title
        # ws= wb[args.title]
        if args.title:
            ws.title = args.title
        if not args.quiet:
            print('Appending to {}'.format(args.output))
    else:
        wb = Workbook()
        wb2 = Workbook()
        ws = wb.active
        ws.title = args.title
    k = 0
    j = 0
    h = []
    for z in m:
        h.append(z[2][1])
    # print(len(sorted(m, reverse=True, key=lambda x: x[7])))
    # print((sorted(m, reverse=True, key=lambda x: x[7])))
    print("Rendering the Data into XLSX Form...")
    g3mp2jump = False
    for l in tqdm(sorted(m, reverse=False, key=tryZPE), total=len(m)):
        ws.cell(column=k + 1, row=j + 1, value='{}'.format(l[0]))  # print title of the block. (filename)
        pathname = ws.cell(column=k + 1, row=j + 2, value='{}'.format(l[1]))  # print title of the block. (filename/path?)
        pathname.alignment = Alignment(horizontal='fill')
        for i in range(2, len(l)):
            ws.cell(column=k + 1, row=j + i + 1, value='{}'.format(l[i][0]))  # "X". the type of data. SCF Done, etc.
            numdata = ws.cell(column=k + 2, row=j + i + 1)  # "Y". the data. Raw numbers.
            numdata.value = '{}'.format(l[i][1])  # "Y". the data. Raw numbers.
            numdata.data_type = "n"
            # numdata.number_format = "0.0"
            compare = ws.cell(column=k + 5, row=j + 3, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(k + 2), j + 3, 3, get_column_letter(2)))
            compare.alignment = Alignment(horizontal='center')
            compare.number_format = "0.0"
            ws.cell(column=k + 5, row=j + 4, value='^^^ vs GM SCF')  # compare_title
        for r in range(8, i + 2):
            ws.cell(column=k + 3, row=j + 3, value=u'\u0394S (cal/mol)')  # dels cal title
            ws.cell(column=k + 3, row=j + 4, value=u'\u0394S (J/mol)')  # dels kJ/mol title
            ws.cell(column=k + 3, row=j + 7, value='kcal/mol')  # kcm title
            ws.cell(column=k + 4, row=j + 7, value='kJ/mol')  # kjm title
            ws.cell(column=k + 5, row=j + 7, value='eV')  # eV title
            ws.cell(column=k + 3, row=j + 6, value='S=>')  # S eq. title
            dScal___ = ws.cell(column=k + 4, row=j + 3, value='=({0}{3}-${2}${1})*627510'.format(get_column_letter(k + 4), 6, get_column_letter(4), j + 6))  # del S cal
            dSkJmol_ = ws.cell(column=k + 4, row=j + 4, value='={0}{1}*4.184'.format(get_column_letter(k + 4), j + 3))  # del S kJ/mol
            datatype = ws.cell(column=k + 4, row=j + 5, value='singlet')  # Temporary string that may change later...
            Seq_____ = ws.cell(column=k + 4, row=j + 6, value='=({0}{2}-{0}{1})/-298.15'.format(get_column_letter(k + 2), j + i, j + i + 1))  # S eq
            kcalmol_ = ws.cell(column=k + 3, row=j + r, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(k + 2), r + j, r, get_column_letter(2)))  # kcal/mol
            kJmol___ = ws.cell(column=k + 4, row=j + r, value='={0}{1}*4.184'.format(get_column_letter(k + 3), r + j))  # kJ/mol
            eV______ = ws.cell(column=k + 5, row=j + r, value='={0}{1}/23.0609'.format(get_column_letter(k + 3), r + j))  # eV
            dScal___.alignment = Alignment(horizontal='center')
            dScal___.number_format = "0.0"
            dSkJmol_.alignment = Alignment(horizontal='center')
            dSkJmol_.number_format = "0.0"
            datatype.alignment = Alignment(horizontal='center')
            kcalmol_.alignment = Alignment(horizontal='center')
            kcalmol_.number_format = "0.0"
            kJmol___.alignment = Alignment(horizontal='center')
            kJmol___.number_format = "0.0"
            eV______.alignment = Alignment(horizontal='center')
            eV______.number_format = "0.0"
            Seq_____.alignment = Alignment(horizontal='center')
        if 'G3MP2 Free Energy' in l[i][0]:
            g3mp2jump = True
        # else:
            # g3mp2jump = False

        k = k + 6
        if k == 18:
            # change k == X to alter number of blocks displayed across
            j = j + 12
            k = 0
            if g3mp2jump:
                j = j + 4

    ws2 = wb.create_sheet(title="histogram data")
    k = 0
    j = 0
    # print(m)
    titleheader = ws2.cell(column=k + 1, row=j + 1, value="Path, Filename")
    titleheader.alignment = Alignment(horizontal='center')
    scfheader = ws2.cell(column=k + 2, row=j + 1, value="SCF E")
    if scan:
        scfheader.value = "initial energy"
    scfheader.alignment = Alignment(horizontal='center')
    scfcmpheader = ws2.cell(column=k + 3, row=j + 1, value="E kcal/mol")
    scfcmpheader.alignment = Alignment(horizontal='center')
    if scan:
        scanpeakheader = ws2.cell(column=k + 5, row=j + 1, value="Scan Peak")
        scanpeakheader.alignment = Alignment(horizontal='center')
        relinitheader = ws2.cell(column=k + 6, row=j + 1, value="Rel. to Init. Geom.")
        relinitheader.alignment = Alignment(horizontal='center')
        scanstepheader = ws2.cell(column=k + 7, row=j + 1, value="Peak Step")
        scanstepheader.alignment = Alignment(horizontal='center')
    if any('zero' in tryZPE(i)[0] for i in m):
        e_zpeheader = ws2.cell(column=k + 4, row=j + 1, value="E_zpe (H)")
        e_zpeheader.alignment = Alignment(horizontal='center')
        e_zpecmpheader = ws2.cell(column=k + 5, row=j + 1, value="E_zpe kcal/mol")
        e_zpecmpheader.alignment = Alignment(horizontal='center')
        delGheader = ws2.cell(column=k + 6, row=j + 1, value=u"\u0394G (H)")
        delGheader.alignment = Alignment(horizontal='center')
        delGcmpheader = ws2.cell(column=k + 7, row=j + 1, value=u"\u0394G kcal/mol")
        delGcmpheader.alignment = Alignment(horizontal='center')
        delHheader = ws2.cell(column=k + 8, row=j + 1, value=u"\u0394H (H)")
        delHheader.alignment = Alignment(horizontal='center')
        delHcmpheader = ws2.cell(column=k + 9, row=j + 1, value=u"\u0394H kcal/mol")
        delHcmpheader.alignment = Alignment(horizontal='center')
        if ('G3MP2 Energy' in tryZPE(i)[0] for i in m):
                g3mp2ZPEheader = ws2.cell(column=k + 10, row=j + 1, value="G3MP2 E_zpe (H)")
                g3mp2ZPEheader.alignment = Alignment(horizontal='center')
                g3mp2ZPEcmpheader = ws2.cell(column=k + 11, row=j + 1, value="G3MP2 E_zpe kcal/mol")
                g3mp2ZPEcmpheader.alignment = Alignment(horizontal='center')
                g3mp2dHheader = ws2.cell(column=k + 12, row=j + 1, value="G3MP2 Enthalpy_zpe (H)")
                g3mp2dHheader.alignment = Alignment(horizontal='center')
                g3mp2dHcmpheader = ws2.cell(column=k + 13, row=j + 1, value="G3MP2 Enthalpy_zpe kcal/mol")
                g3mp2dHcmpheader.alignment = Alignment(horizontal='center')
                g3mp2dGheader = ws2.cell(column=k + 14, row=j + 1, value="G3MP2 Free Energy_zpe (H)")
                g3mp2dGheader.alignment = Alignment(horizontal='center')
                g3mp2dGcmpheader = ws2.cell(column=k + 15, row=j + 1, value="G3MP2 Free Energy_zpe kcal/mol")
                g3mp2dGcmpheader.alignment = Alignment(horizontal='center')

    j = 1
    for q in tqdm(sorted(m, reverse=False, key=tryZPE), total=len(m)):
        # print(tryZPE(q))
        pathname2 = ws2.cell(column=k + 1, row=j + 1, value='{}'.format(q[1]))  # print title of the block. (filename/path?)
        pathname2.alignment = Alignment(horizontal='fill')
        # for i in range(2, len(q)):
        # numdata2 = ws2.cell(column=k + 2, row=j + 1)
        numdata2 = ws2.cell(column=k + 2, row=j + 1, value=float('{}'.format(q[2][1])))
        # numdata2.set_explicit_value(value='{}'.format(tryZPE(q)[1]), data_type='n')  # "Y". the data. Raw numbers.
        # numdata2.set_explicit_value(value='{}'.format(q[2][1]), data_type='n')  # "Y". the data. Raw numbers.
        # numdata2.number_format = "0.0"
        compare2 = ws2.cell(column=k + 3, row=j + 1, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(2), j + 1, 2, get_column_letter(2)))
        compare2.alignment = Alignment(horizontal='center')
        compare2.number_format = "0.0"
        # delG = ws2.cell(column=k + 4, row=j + 1)
        # print('\n', q[-1][0], q[-2][0], q[-3][0])
        # print(q)
        # if "thermal Free" in q[-1][0] or "G3MP2 (0 K)" in q[-4][0]:
        try:
            if "thermal Free" in q[-1][0] or "thermal Free" in q[-5][0]:
                # print('hit thermo', q[0])
                # print(tryZPE(q))
                # print('hit thermo', q[0])
                e_zpe = ws2.cell(column=k + 4, row=j + 1)
                e_zpe.set_explicit_value(value='{}'.format(q[7][1]), data_type='n')  # "Y". the data. Raw numbers.
                compare3 = ws2.cell(column=k + 5, row=j + 1, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(4), j + 1, 2, get_column_letter(4)))
                compare3.alignment = Alignment(horizontal='center')
                compare3.number_format = "0.0"
                delG = ws2.cell(column=k + 6, row=j + 1)
                # print(type(float(q[-1][1])))
                delG.set_explicit_value(value='{}'.format(q[10][1]),
                                        data_type='n')
                compare4 = ws2.cell(column=k + 7, row=j + 1, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(6), j + 1, 2, get_column_letter(6)))
                compare4.alignment = Alignment(horizontal='center')
                compare4.number_format = "0.0"
                # delG.number_format = "0.0"
                delH = ws2.cell(column=k + 8, row=j + 1)
                delH.set_explicit_value(value='{}'.format(q[9][1]), data_type='n')
                compare5 = ws2.cell(column=k + 9, row=j + 1, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(8), j + 1, 2, get_column_letter(8)))
                compare5.alignment = Alignment(horizontal='center')
                compare5.number_format = "0.0"
            # elif "thermal Free" not in q[-1][0] and "G3MP2 (0 K)" not in q[-4][0]:
        # try:
            # if "thermal Free" not in q[10][0]:
                # pass
        except IndexError:
                delG = ws2.cell(column=k + 4, row=j + 1)
                delG.value = "No freq"
        try:
            if "G3MP2 (0 K)" in q[-4][0]:
                g3mp2e_zpe = ws2.cell(column=k + 10, row=j + 1)
                g3mp2e_zpe.set_explicit_value(value='{}'.format(q[-4][1]), data_type='n')
                compare6 = ws2.cell(column=k + 11, row=j + 1, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(10), j + 1, 2, get_column_letter(10)))
                compare6.alignment = Alignment(horizontal='center')
                compare6.number_format = "0.0"
                g3mp2e_dH = ws2.cell(column=k + 12, row=j + 1)
                g3mp2e_dH.set_explicit_value(value='{}'.format(q[-2][1]), data_type='n')
                compare7 = ws2.cell(column=k + 13, row=j + 1, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(12), j + 1, 2, get_column_letter(12)))
                compare7.alignment = Alignment(horizontal='center')
                compare7.number_format = "0.0"
                g3mp2e_dG = ws2.cell(column=k + 14, row=j + 1)
                g3mp2e_dG.set_explicit_value(value='{}'.format(q[-1][1]), data_type='n')
                compare8 = ws2.cell(column=k + 15, row=j + 1, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(14), j + 1, 2, get_column_letter(14)))
                compare8.alignment = Alignment(horizontal='center')
                compare8.number_format = "0.0"
        except IndexError:
            pass
        if scan:
            # print(q)
            try:
                scandata = ws2.cell(column=k + 5, row=j + 1)
                scandata.set_explicit_value(value='{}'.format(q[2][2][0]), data_type='n')  # "Y". the data. Raw numbers.
                scancompare = ws2.cell(column=k + 6, row=j + 1, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(5), j + 1, 2, get_column_letter(2)))
                scancompare.alignment = Alignment(horizontal='center')
                scancompare.number_format = "0.0"
                scanstep = ws2.cell(column=k + 7, row=j + 1)
                scanstep.set_explicit_value(value='{}'.format(q[2][2][1]), data_type='n')
                scanstep.alignment = Alignment(horizontal='center')
                scanstep.number_format = "0"
            except:
                pass



        j = j + 1

    if args.input and args.output == 'SCFsheet.xlsx':
        print("Saving to " + args.input + "...")
        wb.save(args.input)
    else:
        print("Saving to " + args.output + "...")
        wb.save(args.output)


def main():
    if args.errors:
        scanlog(args.path)
        checkerror()
    elif args.run or args.force:
        if os.path.exists(args.output) and not args.force:
            print(
                "This output file exists already. Appending is experimental, thus, please confim with -f|--force.")
        else:
            # if sys.argv:
            #     scanlog(sys.argv[1])
            # else:
            #     scanlog(args.path)
            scanlog(args.path)
            makexlsx()
            if not args.quiet:
                checkerror()
    else:
        parser.print_help()

if __name__ == "__main__":
    main()
