#!/usr/bin/env python3

import glob
import sys
import argparse
from math import sqrt
import re
import os
import shutil

coords = []


parser = argparse.ArgumentParser(description='Takes DCD files, makes pdb files, makes gjf from pdb.')
parser.add_argument('-d', '--distance', dest='D', help='Read dcd files, convert to pdb. May need to alter pdb, so run without this option after editing.', action='store_true', required=False)
parser.add_argument('-a', '--angle', dest='A', help='Make gjf from pdb files. pdb needs to be generated (and/or edited) prior.', action='store_true', required=False)
parser.add_argument('-s', '--spin-mult', dest='SM', help='specify the spin and multiplicity of the gjf output.')
# args = parser.parse_args()


p = re.compile('^[- ]?\d\s\d$')
hcoord = ""


def readbonds():
    atomcoords = []
    # print(sys.argv[1])
    for i, j in zip(glob.glob(sys.argv[1]), range(len(glob.glob(sys.argv[1])))):
        # print(i, j)
        with open(i) as gjf:
            # for line in gjf:
            #     if "CRYST1" in line:
            #         break
            for line in gjf:
                m = p.match(line)
                if m:
                    break
            for i, line in enumerate(gjf):
                elementtype = ""
                # if "ATOM" in line:
                if line.strip().split():
                    s = line.strip().split()
                    # print(type(s))
                    if s[0].startswith(("CL", "Cl", "17")):
                        elementtype = "Cl"
                    elif s[0].startswith(("NA", "Na", "11")):
                        elementtype = "Na"
                    elif s[0].startswith(("P", "15")):
                        elementtype = "P"
                    elif s[0].startswith(("N", "7")):
                        elementtype = "N"
                    # elif "CA" in s[2]:
                    #    elementtype = "Ca"
                    elif s[0].startswith(("C", "6")):
                        elementtype = "C"
                    elif s[0].startswith(("H", "1")):
                        elementtype = "H"
                    elif s[0].startswith(("O", "8")):
                        elementtype = "O"
                    else:
                        if not s:
                            continue
                        elementtype = s[0]
                    # continue
                # print(elementtype)
                atomcoords.append([elementtype + str(i + 1), s[1], s[2], s[3]])
                coords.append(atomcoords)
                # print(atomcoords)


def ii(x, y):
    return float(x[y])
# print(coords)
# print(type(int(sys.argv[2])))


def copygjf(path, oxygen):
    if os.path.isfile(path):
        newfile = path.rsplit(".", 1)[0] + "_" + "H_at_" + oxygen + ".gjf"
        shutil.copy2(path, newfile)
    with open(newfile, "rb+") as nf:
        try:
            nf.seek(-1, 2)
            # print("seeking")
            while nf.read(1) == "\n":
                nf.seek(-2, 1)
        except IOError:
            print("ERROR!")
            nf.seek(0)
        else:
            nf.seek(-1, 2)
        nf.write((" H " + " ".join(map(str, hcoord)) + "\n\n").encode("utf-8"))
        print(newfile + " Created!")

    # checkpoint.append(os.path.basename(sys.argv[2]).rsplit(".", 1)[0] + ".chk")


def writenew(hcoord):
    pass


def nhydrogen(u, v, w):
    global hcoord
    ccbondLength = sqrt(((ii(u, 1) - ii(v, 1))**2) + (ii(u, 2) - ii(v, 2))**2 + (ii(u, 3) - ii(v, 3))**2)
    t = 1 / ccbondLength
    xrp = (1 - t) * ii(u, 1) + (t * ii(v, 1))
    yrp = (1 - t) * ii(u, 2) + (t * ii(v, 2))
    zrp = (1 - t) * ii(u, 3) + (t * ii(v, 3))
    # TODO THIS ONE WORKS!
    xh = ii(w, 1) - xrp + ii(u, 1)
    yh = ii(w, 2) - yrp + ii(u, 2)
    zh = ii(w, 3) - zrp + ii(u, 3)
    # print("The coordanates of the desired proton")
    # print("H", xh, yh, zh)
    hcoord = (xh, yh, zh)
    # print("The coordanates of the reference. 1 angstrom from C2 towards C1")
    # print(xrp, yrp, zrp)


# bondLength = sqrt(((i[1] - j[1])**2) + (i[2] - j[2])**2 + (i[3] - j[3])**2)
# bondLength = sqrt(((ii(i, 1) - ii(j, 1))**2) + (ii(i, 2) - ii(j, 2))**2 + (ii(i, 3) - ii(j, 3))**2)
# print(bondLength)
readbonds()

# print("Run this program by giving the indicies of the desired atoms, in order: carbonyl/carboxyl carbon,"
      # "ipso carbon, carbonyl/carboxyl oxygen. Otherwise, errors happen")
i = coords[0][int(sys.argv[2]) - 1]
j = coords[0][int(sys.argv[3]) - 1]
k = coords[0][int(sys.argv[4]) - 1]

# print(coords[0][int(sys.argv[2]) - 1])
# print(k[0])

nhydrogen(i, j, k)
copygjf(sys.argv[1], k[0])
print("Make sure to rename the output files so as the indicies are just before .gjf!")
# print(" H " + " ".join(map(str, hcoord)) + "\n")
