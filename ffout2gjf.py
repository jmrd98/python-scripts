#!/usr/bin/env python3

# from fafoom import MoleculeDescription, Structure, selection, print_output,\
# remover_dir, set_default, file2dict
# import fafoom.run_utilities as run_util
from copy import deepcopy as deepcopy
import sys
# import os
import argparse
# import shutil
import re
from math import log

blacklist = []

gjf = """%nprocshared={nproc}
%mem={mem}{chk}
#T {opt} {model}{basis_set}{extras} geom=nocrowd

{title}

{charge} {spin}
{data}"""

arraysub = """#!/bin/bash
#SBATCH -J {0}_array0
#SBATCH --ntasks={1}
#SBATCH --nodes=1
#SBATCH --mem-per-cpu={2}
#SBATCH --time=0-168:00:00
#SBATCH --export=all
#SBATCH --output={0}%a.out
#SBATCH --error={0}%a.out
module unload gaussian
module load gaussian-09-e.01
[ ! -f {0}$SLURM_ARRAY_TASK_ID.gjf ] && {{ echo "Script: Array object not found, skipping"; exit 123; }}
\\time -f '%Us User Time\\n%Ss System Time\\n%E Total Time Elapsed\\n%P CPU\\n%M Max Resident Set Size (kB)' g09 {0}$SLURM_ARRAY_TASK_ID.gjf"""


class format_dict(dict):
    def __missing__(self, key):
        return ""


title = "Created from "  # + str(os.path.basename(sys.argv[1]))
nprocshared = "1"
mem = "2gb"
# optoptions = ['', 'rcfc', 'foo']
opts = {"opt": "opt", "sp": "", "optfreq": "opt freq=noraman"}
freqs = {"freq": "freq=noraman"}
model = format_dict(
    {"b3lyp": "b3lyp", "m062x": "m062x", "pm6": "pm6", "hf": "hf"})
basis_set = {"3-21g": "/3-21g", "6-31g(d)": "/6-31g(d)",
             "6-31+g(d,p)": "/6-31+g(d,p)", "6-31++g(d,p)": "/6-31++g(d,p)"}
pluses = {"p": "+", "pp": "++", "no": ""}
extras = {"dp": "(d,p)", "no": ""}
checkpoint = []


class Structure:
    """Create 3D structures."""
    index = 0
    newline = "NEWLINE"

    def __init__(self, arg=None, **kwargs):
        """Initialize the 3D structure: (1) from MoleculeDescription class
        object or from (2) from previously created object of the Structure
        class. Any present and valid keyword args overwrite the old values.
        Warning: there may be more attributes in the (2) case."""
        self.mol_info = arg
        if isinstance(arg, Structure):

            self.mol_info = arg.mol_info
            Structure.index += 1
            self.index = Structure.index
            for att_name in arg.__dict__.keys():
                if att_name not in ["mol_info", "index"]:
                    setattr(self, str(att_name), deepcopy(
                        getattr(arg, str(att_name))))

        for key in kwargs.keys():
            if key != "index":
                if hasattr(self, str(key)):
                    pass
                    # print_output("Overwriting the value for keyword "+str(key))
                    # print_output("Old value: "+str(getattr(self, str(key))) +
                    # ", new value: "+str(kwargs[key]))
                if key in ["sdf_string", "initial_sdf_string"]:
                    setattr(self, str(key),
                            kwargs[key].replace(Structure.newline, "\n"))
                else:
                    setattr(self, str(key), kwargs[key])


mol = []
# with open("backup_mol.dat", 'r') as inf:
# mol = eval(inf.readline())


# def get_ind_from_sdfline(sdf_line):
#     """Extract the indicies from the sdf string (for molecules with more than
#     99 atoms)"""
#     l = len(sdf_line.split()[0])
#     if l < 4:
#         ind1 = int(sdf_line.split()[0])
#         ind2 = int(sdf_line.split()[1])
#     else:
#         list_ind = list(sdf_line.split()[0])
#         if len(list_ind) == 5:
#             ind1 = int(list_ind[0] + list_ind[1])
#             ind2 = int(list_ind[2] + list_ind[3] + list_ind[4])
#         if len(list_ind) == 6:
#             ind1 = int(list_ind[0] + list_ind[1] + list_ind[2])
#             ind2 = int(list_ind[3] + list_ind[4] + list_ind[5])

#     return ind1, ind2


class Generate_gjf:
    """docstring for Generate_gjf"""

    def __init__(self, arg=None, **kwargs):
        self.arg = arg
        self.block = []
        self.coord = []
        self.blacklist = []
        with open("backup_blacklist.dat", 'r') as inf:
            for line in inf:
                self.blacklist.append(eval(line))

        parser = argparse.ArgumentParser(description='Turns blacklist output from fafoom into \
                                         turnkey gaussian arrays via sorcery.')
        parser.add_argument(
            '-s', '--stem', help='Basename to give new files. Required', required=True)
        parser.add_argument('-t', '--type', help='specify either opt, optfreq,\
                            or sp job type. Default: Opt', required=False, default='opt')
        parser.add_argument('-c', '--charge', help='specify molecule charge. \
                            Required', required=True)
        parser.add_argument('-p', '--proc', help='number of processors for job.\
                            Default: 1', default=1, required=False)
        parser.add_argument('-m', '--mem', help='memory for job. Default: 1gb', default='2gb',
                            required=False)
        parser.add_argument('--multiplicity', help='Set multiplicity manually.\
                            Default 1.', default=1, required=False)
        parser.add_argument('-M', '--model',
                            help=('Pick one of: {}. \
                                  Default: hf'.format(', '.join(sorted(
                                [i for i in model.keys()])))),
                            default='hf', required=False)
        parser.add_argument('-b', '--basis',
                            help=('pick one of: {}. \
                                  Default: 3-21g'.format(', '.join(sorted(
                                [i for i in basis_set.keys()])))),
                            default='3-21g', required=False)

        if len(sys.argv) == 1:
            parser.print_help()
            sys.exit(1)
        self.args = parser.parse_args()
        self.output_format = format_dict({
            "opt": opts[self.args.type],
            # "optoptions": optoptions[0],
            # "freq": freqs["freq"],
            # "mem": mem,
            "mem": self.args.mem,
            # "nproc": nprocshared,
            "nproc": self.args.proc,
            "model": model[self.args.model],
            "basis_set": basis_set[self.args.basis],
            "extras": extras["no"],
            "pluses": pluses["no"],
            "title": title,
            # "chk": checkpoint[1],
            # "chk": ,
            "charge": self.args.charge,
            # "chargespin": "0 1"
            # "spin": "1",
            "spin": self.args.multiplicity,
            # "data": "\n".join('\t'.join(i) for i in l)
            # "data": xyz
        })

    def get_ind_from_sdfline(self, sdf_line):
        """Extract the indicies from the sdf string (for molecules with more than
        99 atoms)"""
        length = len(sdf_line.split()[0])
        if length < 4:
            ind1 = int(sdf_line.split()[0])
            ind2 = int(sdf_line.split()[1])
        else:
            list_ind = list(sdf_line.split()[0])
            if len(list_ind) == 5:
                ind1 = int(list_ind[0] + list_ind[1])
                ind2 = int(list_ind[2] + list_ind[3] + list_ind[4])
            if len(list_ind) == 6:
                ind1 = int(list_ind[0] + list_ind[1] + list_ind[2])
                ind2 = int(list_ind[3] + list_ind[4] + list_ind[5])

        return ind1, ind2

    def sdf2xyz(self, sdf_string):
        """Convert a sdf string to a xyz string."""
        atoms = self.get_ind_from_sdfline(sdf_string.split('\n')[3])[0]
        coord = [str(atoms)]  # +('\n')]
        for i in range(4, 4 + atoms):
            x = float(sdf_string.split('\n')[i].split()[0])
            y = float(sdf_string.split('\n')[i].split()[1])
            z = float(sdf_string.split('\n')[i].split()[2])
            name = sdf_string.split('\n')[i].split()[3]
            coord.append('\n%2s%10.4f%10.4f%10.4f' % (name, x, y, z))
        coord.append('\n')
        xyz_string = ''.join(coord)
        return xyz_string

    def sdf2gjf(self, sdf_string):
        """Convert a sdf string to a gjf string."""
        # print(sdf_string)
        atoms = self.get_ind_from_sdfline(sdf_string.split('\n')[3])[0]
        # coord = [str(atoms)]  # +('\n')]
        self.coord = []
        for i in range(4, 4 + atoms):
            x = float(sdf_string.split('\n')[i].split()[0])
            y = float(sdf_string.split('\n')[i].split()[1])
            z = float(sdf_string.split('\n')[i].split()[2])
            name = sdf_string.split('\n')[i].split()[3]
            self.coord.append('%2s%10.4f%10.4f%10.4f\n' % (name, x, y, z))
        # coord.append('\n')
        xyz_string = ''.join(self.coord)
        # print xyz_string
        return xyz_string
    # print(get_ind_from_sdfline(blacklist[0].__dict__['sdf_string']))

    def getstruct(self):
        for index in self.blacklist:
            # print(index)
            self.block.append(index.__dict__['sdf_string'])
            # return index.__dict__['sdf_string']

    def printblock(self):
        print("hit printblock")
        for i in self.block:
            print(i)

    def printgjf(self):
        for i in self.block:
            # print(i)
            # print(self.sdf2gjf(i))
            print(gjf.format(data=self.sdf2gjf(i), **self.output_format))
            # print(self.args.stem)

    def writegjf(self):
        for index, i in enumerate(self.block, 1):
            self.output_format['chk'] = '\n%chk=' + self.args.stem + str(index) + '.chk' if\
                self.args.type == 'optfreq' else ''
            with open(self.args.stem + str(index) + '.gjf', 'w') as f:
                f.write(gjf.format(data=self.sdf2gjf(i), **self.output_format))
                f.write('\n')
        # print(self.args.stem)
        print("Total files: " + str(index))

    def submem(self, memint):
        x = (re.findall("\d", memint))
        x = int(x[0])
        return int(x * 1024 + int(768 * max(1, log(x)))) // int(self.args.proc)

    def writesub(self):
        with open((self.args.stem + '.sub'), 'w') as s:
            s.write(arraysub.format(self.args.stem, self.args.proc,
                                    self.submem(self.args.mem)))

# xyz = sdf2gjf(blacklist[0].__dict__['sdf_string'])
# xyz = sdf2gjf(getstruct())


# print(get_ind_from_sdfline(blacklist[0].__dict__['sdf_string'].split('\n')[3])[0])
# xyz = sdf2gjf(blacklist[0].__dict__['sdf_string'])
# print(gjf.format(**output_format))


output = Generate_gjf()
# print(output.blacklist[0].__dict__['sdf_string'])
output.getstruct()
output.writegjf()
output.writesub()
