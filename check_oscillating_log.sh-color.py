#!/usr/bin/env python3

from colored import fg, bg, attr, stylize
from itertools import tee
import sys

s = []


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def testing():
    with open("scf-dump.txt", "r") as f:
        for line in f:
            # print(line.split(), end='')
            s.append(line.strip().split())
            # print(s)


def testing2():
    for line in sys.stdin:
        # print(line.split(), end='')
        s.append(line.strip().split())


redpair = fg(15), bg(1)
greenpair = fg(15), bg(34)
testing2()
# print(s[0])
# print([' '.join(i) for i in s])
# print(stylize(' '.join(s[0][0:4]), greenpair))
# print(stylize(' '.join(s[0][0:4]), redpair))
first = True
for k, (i, j) in enumerate(pairwise(s)):
    if first:
        first = False
        print(' '.join(i[:4]), stylize(i[4], greenpair), ' '.join(i[5:]))
    # print(i[4], j[4], k)
    if j[4] >= i[4]:
        # print(i[4], 'i', j[4], 'j')
        # print(' '.join(i[:4]), stylize(i[4], greenpair), ' '.join(i[5:]), 'if')
        print(' '.join(j[:4]), stylize(j[4], greenpair), ' '.join(j[5:]))
        # print(stylize(i[4], greenpair))
        # sys.stdout.write((stylize(i[4], '\n'), greenpair))
    else:
        # print(i[4], 'i', j[4], 'jee')
        print(' '.join(j[:4]), stylize(j[4], redpair), ' '.join(j[5:]))
        # print(' '.join(j[:4]), stylize(j[4], redpair), ' '.join(j[5:]), 'else')
        # sys.stdout.write(str(stylize(i[4], '\n'), redpair))

