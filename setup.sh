#!/bin/sh
mkdir -p $HOME/bin

if [ -d $HOME/bin/.git ]; then
	echo "$HOME/bin appears to already contain a git repository. We are likely already installed."
	exit 1
else

env git clone --depth=1 https://jmrd98@bitbucket.org/jmrd98/python-scripts.git $HOME/bin

cp -a $HOME/.bash_profile $HOME/.bash_profile-bak

cat << EOF >>$HOME/.bashrc
module load python/2.7.9 scipy
EOF

cat << EOF >>$HOME/.bash_profile
PATH=$PATH:$HOME/bin:$HOME/.local/bin

export PATH
EOF

source $HOME/.bash_profile

echo 'Install complete! Basic documentation available in ~/bin/readme.md'
fi;
