#!/bin/bash

for i in $@
do
cp -a $i $i.origBS
sed -i -e 's/b3lyp/m062x/' $i
touch -r $i.origBS $i
done
