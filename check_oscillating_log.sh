#!/bin/sh
grep --color=auto -E -A 3 'Maximum Force|Optimization completed' $1.log
#grep --color=auto -E 'Maximum Force|Optimization completed' $1
read -n1 -r
plot $2 -y "$(grep 'Maximum Force' $1.log |awk '{print $3}'|sed 's/\*\*\*\*\*\*\*\*/1.0/')"
read -n1 -r
#plot $2 -y "$(grep -E "SCF.*cycles" $1 |awk '{print $5}'|sed 's/\*\*\*\*\*\*\*\*/1.0/')"
#grep -E "SCF.*cycles" $1.log
grep -E "SCF.*cycles" $1.log | check_oscillating_log.sh-color.py
read -n1 -r
echo $(grep -E "SCF.*cycles" $1.log |awk '{print $5}') | xargs -I {} plot -y "{}"
echo $2 $3
