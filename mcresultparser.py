#!/usr/bin/env python3

import glob
# import os
import csv
import sys


s = set()
slist = []
zlist = []
resultlist = {}


def getunique():
    g = glob.glob("*mobresults")
    for i in g:
        s.add(i.rsplit("_", 1)[0])
    # print(list(s))
    # print(s)
    slist = list(s)
    # print(slist)
    # print((slist))
    # for i in slist:
    # with open("temp.txt", 'w') as file:
    for z in slist:
        # print(z)
        # print((glob.glob(z.rsplit("_", 1)[0] + "*.mobresults")[0]))
        zlist.append(glob.glob(z + "_*.mobresults"))
    global lz
    lz = len(zlist[0])


def parseoutput():
    tempdict = {}
    filename, ccs, sd = [], [], []
    tempcontainer = {}
    # print(sorted(zlist))
    for e, i in enumerate(sorted(zlist)):
        # print(i[2])
        ele = sorted(list(s))[e]
        # print(ele)
        tempdict[ele] = {}
        # print(tempdict)
        # print(i)
        for j in i:
            # tempcontainer.update({j: ({})})
            tempdict[ele].update({j: ({})})
            # print(tempdict[ele].update({j: ({})}))
            with open(j) as output:
                # filename.append(j)
                for line in output:
                    if "average TM cross section" in line:
                        # print(j)
                        # print(line)
                        # ccs.append(line.split('=')[1].strip())
                        tempdict[ele][j].update({'ccs': line.split('=')[1].strip()})
                        break
                        pass
                    if "standard deviation =" in line:
                        tempdict[ele][j].update({'sd': line.split('=')[1].strip()})
                        pass
            filename.append(i)
            # print(tempcontainer)
            # tempdict.update()
            tempcontainer = {}
        # print(tempdict[ele][j], j)
        # tempdict.update({'ccs': ccs, 'filename': filename, 'sd': sd})
        resultlist.update(tempdict)
        filename, ccs, sd = [], [], []
        tempdict = {}

    # print(sorted(list(s)))
    pass


getunique()
parseoutput()
# print(len(resultlist[0].get('filename')))
# print(zlist)
# print((resultlist['TGAcid_trimer_namd_H_at_O59-m06_opt1-311_O59']))
# print([x for x in resultlist['TGAcid_trimer_namd_H_at_O59-m06_opt1-311_O59'][x]['ccs']]
# print((resultlist).get('TGAcid_trimer_namd_H_at_O38-m06_opt3-351_O38'))
fields = ['filename', 'ccs', 'sd']
# for i in resultlist:
#     for key, val in sorted(resultlist[i].items()):
#         # print(key, val['ccs'], val['sd'])
#         row = {'filename': key}
#         row.update(val)
#         print(row)
j = 0
l = 0
oldform = lambda n: (38 * n) + (18 * (-1)**n) - 19
# Thanks wolframalpha...!

def form(n):
    return int((lz + 1) / 2 * n + ((-1)**n) * (lz - 3) / 4 - (lz + 1) / 4)
    pass

for c, i in enumerate(resultlist, start=1):
    if l is 0:
        l = c
    print(c, 'c')
    print(l, 'l')
    print(j, 'j')
    print(form(l), 'form1', form(l + 1), 'form2')
    with open(i + '.csv', 'w') as f, open('master.csv', 'a') as ff:
        for k, v in sorted(resultlist[i].items()):
            w = csv.DictWriter(f, fields)
            ww = csv.DictWriter(ff, fields)
            row = {'filename': k}
            row.update(v)
            w.writerow(row)
            ww.writerow(row)
            if c is 0:
                j = j + 1
            else:
                j = j + c
        w2 = csv.writer(f)
        w2.writerow(['',('=AVERAGE(B'+str(form(1))+':B'+str(form(2))+')'),'=STDEV(B'+str(form(1))+':B'+str(form(2))+')'])
        www = csv.writer(ff)
        # print(j, 'j2')
        www.writerow(['',('=AVERAGE(B'+str(form(l))+':B'+str(form(l+1))+')'),'=STDEV(B'+str(form(l))+':B'+str(form(l+1))+')'])
        l = l+2
        j = c
        c = c+3
        # print(c+2, 'c2')
    # with open(i + '.csv', 'a') as ff:
        # ww = csv.writer(ff)
        # ww.writerow(['',('=AVERAGE(B1:B'+str(j)+')'),'=STDEV(B1:B'+str(j)+')'])


#with open('master' + '.csv', 'w') as f:
#    for i in resultlist:
#        j = 0
#        for k, v in sorted(resultlist[i].items()):
#            w = csv.DictWriter(f, fields)
#            ww = csv.writer(f)
#            row = {'filename': k}
#            row.update(v)
#            w.writerow(row)
            # j = j+1
            # ww.writerow(['',('=AVERAGE(B1:B'+str(j)+')'),'=STDEV(B1:B'+str(j)+')'])
#            j = 0
#with open('master' + '.csv', 'w') as f:
#    for i in glob.glob("*.csv"):
#        with open(i) as ff:
#            for line in ff:
#                f.write(line)

