#!/bin/bash

for i in $@
do
cp -a $i $i.origVerb
sed -i -e 's\#\#T\' $i
touch -r $i.origVerb $i
done
