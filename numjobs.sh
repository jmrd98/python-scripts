#!/bin/bash

squeue1=$(squeue -o "%i %.9P %.8j %.u %.t %.M %.C %.D %.r" -p grethor)
squeue_nproc=$(squeue -o "%.C %.r %.P" |grep None |grep grethor|sed 's/[^0-9]//g' |paste -sd+ |bc)
#squeue_nproc_pri=$(squeue -o "%.C %.r %.P" |grep Priority |grep grethor|sed 's/[^0-9]//g' |paste -sd+ |bc)
#squeue_nproc_res=$(squeue -o "%.C %.r %.P" |grep Resources |grep grethor|sed 's/[^0-9]//g' |paste -sd+ |bc)
#users="bythellb jmrd98 sg4g5 mta35f drs8t2 jfdhf"
users="bythellb jmrd98 sg4g5"
echo -e "Total Processors in use: $squeue_nproc"
echo -e "Total CPU present on Grethor: $(bc <<<"$(egrep edrcompute-20-[0-7] /etc/slurm/nodenames.conf |wc -l)*40")"
for j in $users; do
	echo -e "\n$j"
	b=0
	c=0
	d=0
	variable=$(grep -E "$j.*None"<<<"$squeue1")
	var_pri=$(grep -E "$j.*Priority"<<<"$squeue1")
	var_res=$(grep -E "$j.*Resources"<<<"$squeue1")
	#echo $var_pri|grep -i "\s1...Priority"
	for i in {1..48}; do
	njobs=$(echo "$variable" |grep -i "\s$i...none" |wc -l)
	njobs_proc=$(($i*$(echo "$variable" |grep -i "\s$i...none" |wc -l)))
		#if (( $(($i*$(echo "$variable" |grep -i "$i...none" |wc -l))) > 0)); then
		if (( $njobs_proc > 0)); then
			echo "Number of $i processor jobs: "$njobs""
			echo "Number of $i processor jobs processors in use: $njobs_proc"
			b=$(($njobs_proc+b))
		fi
	done
	for k in {1..48}; do
	njobs_pri=$(echo "$var_pri" |grep -i "\s$k...Priority" |wc -l)
	njobs_pri_proc=$(($k*$(echo "$var_pri" |grep -i "\s$k...Priority" |wc -l)))
	njobs_res=$(echo "$var_res" |grep -i "\s$k...Resources" |wc -l)
	njobs_res_proc=$(($k*$(echo "$var_res" |grep -i "\s$k...Resources" |wc -l)))
		if (( $njobs_pri > 0 )); then
			echo -e "Number of $k processor jobs waiting on Priority: "$njobs_pri""
			echo "Number of $k Processors waiting on Priority: $njobs_pri_proc"
			c=$(($njobs_pri_proc+c))
		fi
		if (( $njobs_res > 0)); then
			echo -e "Number of $k processor jobs waiting on Resources: "$njobs_res""
			echo "Number of $k Processors waiting on Resources: $njobs_res_proc"
			d=$(($njobs_res_proc+d))
		fi
	done
	echo "Total Processors: $b"
	echo -e "Percentage of processors in use: $(printf "%g\n" $(bc -l <<< "scale=3;100*$b/$squeue_nproc"))%"
done
