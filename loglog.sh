#!/bin/bash

find ~/ -iname "*.log" -mtime -${1:-1} -printf "%T+\t%p\n" | sort
