#!/bin/bash

for i in $@
do
cp -a $i $i.origAN
sed -i -e 's/Atom  AN/Atom AN/' $i
touch -r $i.origAN $i
done
