#!/usr/bin/env python
from __future__ import print_function
import sys
import os
import fnmatch
import argparse
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.compat import range
from openpyxl.utils import get_column_letter
from openpyxl.styles import Alignment
import tarfile
from tqdm import tqdm
from decimal import *
import numpy as np

parser = argparse.ArgumentParser(description='Scans *.log files and extracts final SCF energy and Assorted energies. Sorts and produces a XLSX file.')
parser.add_argument('-p', '--path', help='Path to *.log files which are to be scanned. (Defaults to Current Directory.)', default=os.getcwd(), required=False)
parser.add_argument('-l', '--logs', nargs='*', help='A list of one or more *.log files to scan.', required=False)
parser.add_argument('-P', '--print', dest='P', help='Print the list of *.log files scanned. (Not implimented yet.)', action='store_true', required=False)
parser.add_argument('-i', '--input', help='Specify an existing XLSX file to *APPEND* to. (Hopefully)', required=False)
parser.add_argument('-o', '--output', help='Specify a filename for the output file. (Defaults to SCFsheet.xlsx)', default='SCFsheet.xlsx', required=False)
parser.add_argument('-t', '--title', help='Title of the new worksheet in the XLSX file. (Defaults to SCFsheet)', default='SCFsheet', required=False)
parser.add_argument('-f', '--force', '--confirm', help='This flag must be set in order to confirm you want to overwrite the existing file.', action='store_true', required=False)
parser.add_argument('-e', '--errors', help="Don't run; just check for errors in the files", action='store_true')
parser.add_argument('-r', '--run', help="Confirm execution of the command. (So we can print this lovely text by default!)", action='store_true')
parser.add_argument('-q', '--quiet', help="Suppress output/run silently. (I don't know why you would want this, but I need to test the feature somehow.)", action='store_true')
parser.add_argument('-R', '--recursive', help='Scan *.log files for this folder and any subdirectories within it.', action='store_true', required=False)
args = parser.parse_args()

m = []
errors = []

# drop in a random comment...
# print(sys.version_info)


def getlog(path):
    """Is the location specified local or recursive?"""
    # if tarfile.is_tarfile(path, mode='r:*'):
    if args.recursive:
        print("we hit the recursive branch")
        for root, dirs, files in os.walk(path):
            for name in files:
                if fnmatch.fnmatch(name, '*.log'):
                    yield os.path.join(root, name)
    elif args.logs:
        print(path)
        print("we hit the args.logs branch")
        for files in args.logs:
            if fnmatch.fnmatch(files, '*.tar.gz'):
                yield "".join(files)
            elif os.path.isfile(files):
                if fnmatch.fnmatch(files, '*.log'):
                    yield files
    else:
        print("we fell into the last branch")
        for files in os.listdir(path):
            if fnmatch.fnmatch(files, '*.log'):
                yield os.path.join(path, files)


def tarscanlog(path):
    for files in getlog(path):
        print("Opening " + files + " for Reading... Please Stand By.")
        with tarfile.open(files, mode='r:*') as tar:
            print("Scanning Tarball for log files...")
            for d in tqdm(tar, total=len(tar.getnames())):
                # print(tarinfo)
                f = tar.extractfile(d)
                l = []
                s = f.readlines()
                s = [line.decode() for line in s]
                if "Normal" not in s[-1]:
                    errors.extend((os.path.basename(d.name), ": File did not end/end cleanly", "\n"))
                    continue
                scf = []
                for k in s:
                    # print(k)
                    if k.startswith(" SCF"):
                        scf.append(k)
                if not scf:
                    scf.append("SCF = Not_Found")
                l.append(os.path.basename(f.name))
                l.append(d.name)
                l.append([scf[-1].split('=')[0].strip(), scf[-1].split('=')[1].strip().split()[0]])
                # this line is similarly complex to the one below (which was written first) in order to rip out the whitespace similarly.
                if [i for i in s if i.startswith(" Zero-point c")]:
                    mx = max(i for i, line in enumerate(s) if line.startswith(" Zero-point c"))
                else:
                    continue
                # this ugly ass thing replaced the commented code below: we do the comparison, and return the highest/last matching line in the file.
                for j in s[mx:mx + 8]:
                    l.append([j.split('=')[0].strip(), j.split('=')[1].strip().split()[0]])
                    # This line is complex to catch the whitespace and the (Hartree/Particle) suffix on ZPE
                m.append(l)
    #         print(len(l), "L len")
    # print(len(m), "m len")


def scanlog(path):
    # for files in os.listdir(path):
    for files in getlog(path):
        """stick a try/else in here???"""
        # if fnmatch.fnmatch(files, '*.log'):
        l = []
        with open((files), 'rt') as f:
            # print(files, "printing the files in the 'with'")
            # print(f)
            s = f.readlines()
            # print(s)
            # if not e.startswith(" Normal Termination"):
            if "Normal" not in s[-1]:
                errors.extend((os.path.basename(f.name), ": File did not end/end cleanly", "\n"))
                continue
        # for c in s:
        #     if c.startswith("IRC"):
        #         print("hit c irc branch")
        #         errors.extend((os.path.basename(f.name), ": File appears to be an IRC", "\n"))
        #     continue
            # if not s.startswith(" Zero-point"):
            #     errors.extend((os.path.basename(f.name), ": No Zero-point entry found", "\n"))
            #     print("We hit zp not in s")
            #     continue
            scf = []
            # print(s)
            for k in s:
                # print("did we hit k in s?")
                if k.startswith(" SCF"):
                    scf.append(k)
                # else:
                #     pass
                    # scf.append("SCF = Not_Found_,sadness")
            if not scf:
                scf.append("SCF = Not_Found")
            l.append(os.path.basename(f.name))
            l.append(f.name)
            l.append([scf[-1].split('=')[0].strip(), scf[-1].split('=')[1].strip().split()[0]])
            # this line is similarly complex to the one below (which was written first) in order to rip out the whitespace similarly.
            mx = max(i for i, line in enumerate(s) if line.startswith(" Zero-point c"))
            # this ugly ass thing replaced the commented code below: we do the comparison, and return the highest/last matching line in the file.
            # print(mx)
            # for i, line in enumerate(s):
            #     if line.startswith(" Zero-point c"):
            # print(os.getcwd())
            # print(i)
            # for j in s[i:i + 8]:
            for j in s[mx:mx + 8]:
                l.append([j.split('=')[0].strip(), j.split('=')[1].strip().split()[0]])
                # This line is complex to catch the whitespace and the (Hartree/Particle) suffix on ZPE
        m.append(l)
        print(len(l), "L len")
    # print(m, "M list")
    print(len(m), "m len")
# print(errors)


def checkerror():
    print("The following files contained errors and were not processed:\n", ''.join(errors))


def makexlsx():
    if args.input:
        if os.path.exists(args.input):
            wb = load_workbook(args.input, guess_types=False)
            ws = wb.create_sheet()  # title = args.title
            # ws= wb[args.title]
            if args.title:
                ws.title = args.title
    elif os.path.exists(args.output):
        wb = load_workbook(args.output, guess_types=False)
        ws = wb.create_sheet()  # title = args.title
        # ws= wb[args.title]
        if args.title:
            ws.title = args.title
        if not args.quiet:
            print('Appending to {}'.format(args.output))
    else:
        wb = Workbook(guess_types=True)
        ws = wb.active
        ws.title = args.title
    k = 0
    j = 0
    h = []
    for z in m:
        h.append(z[7][1])
    #     testarray = np.empty((0, len(z)))
    #     # testarray = np.append(testarray, [z[7][1]])
    #     # print(Decimal(z[7][1]))
    #     print(type(testarray))
    # # print(m[0][7][1])
    testarray = np.array(h).astype(np.float)
    print(testarray)
    # testarray = np.array(x for x in m[7])
    print(np.histogram(testarray))
    # print(len(sorted(m, reverse=True, key=lambda x: x[7])))
    # print((sorted(m, reverse=True, key=lambda x: x[7])))
    print("Rendering the Data into XLSX Form...")
    for l in tqdm(sorted(m, reverse=True, key=lambda x: x[7]), total=len(m)):
        # print(Decimal(l[7][1]))
        # print(len(l))
        ws.cell(column=k + 1, row=j + 1, value='{}'.format(l[0]))  # print title of the block. (filename)
        pathname = ws.cell(column=k + 1, row=j + 2, value='{}'.format(l[1]))  # print title of the block. (filename/path?)
        pathname.alignment = Alignment(horizontal='fill')
        for i in range(2, len(l)):
            ws.cell(column=k + 1, row=j + i + 1, value='{}'.format(l[i][0]))  # "X". the type of data. SCF Done, etc.
            ws.cell(column=k + 2, row=j + i + 1, value='{}'.format(l[i][1]))  # "Y". the data. Raw numbers.
        for r in range(8, i + 2):
            ws.cell(column=k + 3, row=j + 3, value=u'\u0394S (cal/mol)')  # dels cal title
            ws.cell(column=k + 3, row=j + 4, value=u'\u0394S (J/mol)')  # dels kJ/mol title
            ws.cell(column=k + 3, row=j + 7, value='kcal/mol')  # kcm title
            ws.cell(column=k + 4, row=j + 7, value='kJ/mol')  # kjm title
            ws.cell(column=k + 5, row=j + 7, value='eV')  # eV title
            ws.cell(column=k + 3, row=j + 6, value='S=>')  # S eq. title
            dScal___ = ws.cell(column=k + 4, row=j + 3, value='=({0}{3}-${2}${1})*637510'.format(get_column_letter(k + 4), 6, get_column_letter(4), j + 6))  # del S cal
            dSkJmol_ = ws.cell(column=k + 4, row=j + 4, value='={0}{1}*4.184'.format(get_column_letter(k + 4), j + 3))  # del S kJ/mol
            datatype = ws.cell(column=k + 4, row=j + 5, value='singlet')  # Temporary string that may change later...
            Seq_____ = ws.cell(column=k + 4, row=j + 6, value='=({0}{1}-{0}{2})/-298.15'.format(get_column_letter(k + 2), j + i + 1, j + i + 2))  # S eq
            kcalmol_ = ws.cell(column=k + 3, row=j + r, value='=({0}{1}-${3}${2})*627.51'.format(get_column_letter(k + 2), r + j, r, get_column_letter(2)))  # kcal/mol
            kJmol___ = ws.cell(column=k + 4, row=j + r, value='={0}{1}*4.184'.format(get_column_letter(k + 3), r + j))  # kJ/mol
            eV______ = ws.cell(column=k + 5, row=j + r, value='={0}{1}/23.0609'.format(get_column_letter(k + 3), r + j))  # eV
            dScal___.alignment = Alignment(horizontal='center')
            dScal___.number_format = "0.0"
            dSkJmol_.alignment = Alignment(horizontal='center')
            dSkJmol_.number_format = "0.0"
            datatype.alignment = Alignment(horizontal='center')
            kcalmol_.alignment = Alignment(horizontal='center')
            kcalmol_.number_format = "0.0"
            kJmol___.alignment = Alignment(horizontal='center')
            kJmol___.number_format = "0.0"
            eV______.alignment = Alignment(horizontal='center')
            eV______.number_format = "0.0"
            Seq_____.alignment = Alignment(horizontal='center')

        k = k + 6
        if k == 18:
            # change k == X to alter number of blocks displayed across
            j = j + 12
            k = 0
    if args.input and args.output == 'SCFsheet.xlsx':
        print("Saving to " + args.input + "...")
        wb.save(args.input)
    else:
        print("Saving to " + args.output + "...")
        wb.save(args.output)


def main():
    if args.errors:
        scanlog(args.path)
        checkerror()
    elif args.run or args.force:
        if os.path.exists(args.output) and not args.force:
            print(
                "This output file exists already. Appending is experimental, thus, please confim with -f|--force.")
        else:
            # if sys.argv:
            #     scanlog(sys.argv[1])
            # else:
            #     scanlog(args.path)
            scanlog(args.path)
            makexlsx()
            if not args.quiet:
                checkerror()
    else:
        parser.print_help()

if __name__ == "__main__":
    main()
